package de.haase.persistence.entities;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Customer {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String firstname;
    private int age;
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name="CUSTOMER_ID")
    private List<Address> addresses;

    public Customer(){}

    public Customer(String name, String firstname, int age) {
        this.name = name;
        this.firstname = firstname;
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses= addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id && age == customer.age && name.equals(customer.name) && firstname.equals(customer.firstname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, firstname, age);
    }



}
