package de.haase.persistence.daos;

import de.haase.persistence.entities.Customer;

import java.util.List;

public interface CustomerDao extends Dao<Customer>{

    List<Customer> findByName(String name);
}
