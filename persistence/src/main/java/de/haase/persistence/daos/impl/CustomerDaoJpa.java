package de.haase.persistence.daos.impl;

import de.haase.persistence.daos.CustomerDao;
import de.haase.persistence.entities.Customer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public class CustomerDaoJpa implements CustomerDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Customer> get(long id) {
        return Optional.ofNullable(entityManager.find(Customer.class, id));
    }

    @Override
    public List<Customer> getAll() {
        return entityManager.createQuery("from Customer", Customer.class).getResultList();
    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        entityManager.persist(customer);
        return customer;
    }

    @Override
    @Transactional
    public void update(Customer customer) {
        entityManager.merge(customer);
    }

    @Override
    @Transactional
    public void delete(Customer customer) {
        entityManager.remove(entityManager.contains(customer) ? customer : entityManager.merge(customer));
    }

    @Override
    @Transactional
    public List<Customer> findByName(String name) {
        return entityManager.createQuery("from Customer c where c.name = :name", Customer.class)
                .setParameter("name", name).getResultList();
    }
}
