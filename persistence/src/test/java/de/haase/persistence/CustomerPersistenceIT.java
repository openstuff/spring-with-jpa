package de.haase.persistence;

import de.haase.persistence.daos.CustomerDao;
import de.haase.persistence.entities.Address;
import de.haase.persistence.entities.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@SpringJUnitConfig(PersistenceConfiguration.class)
public class CustomerPersistenceIT {

    @Autowired
    private CustomerDao customerDao;

    private long customerId;


    @BeforeEach
    public void setUp() {
        Customer customer = new Customer( "Doe", "John", 77);
        Address address = new Address("Main Street", "19", "800", "Springfield");
        Address address2 = new Address("Trevor Avnu.", "155", "643", "Hotchester");
        customer.setAddresses(List.of(address, address2));
        customerDao.save(customer);
        customerId = customer.getId();
    }

    @AfterEach
    public void tearDown() {
        Optional<Customer> customer = customerDao.get(customerId);
        customerDao.delete(customer.get());
    }

    @Test
    @DisplayName("Entity should be persisted and retrieved again.")
    public void customerShouldBePersistedAndFindAgain() {
        // When
        Optional<Customer> customer = customerDao.get(customerId);

        // Then
        assertThat(customer.isPresent(), is(true));
        assertThat(customer.get().getName(), is(equalTo("Doe")));
    }

    @Test
    @DisplayName("Entity should be removed again.")
    void customerShouldBeRemoved() {
        // Given
        Customer customer = new Customer("Martin", "Mustermann", 56);
        Address address = new Address("Marktstraße", "7a", "12345", "Berlin");
        customer.setAddresses(List.of(address));

        // When
        customerDao.save(customer);
        // Then
        assertThat(customerDao.get(customer.getId()).isPresent(), is(true));

        // When
        customerDao.delete(customer);
        // Then
        assertThat(customerDao.get(customer.getId()).isPresent(), is(false));
    }

    @Test
    @DisplayName("Get a customer by his name.")
    void getCustomerByName() {
        // When
        Optional<Customer> customer = customerDao.get(customerId);

        // Then
        assertThat(customer.isPresent(), is(true));
    }

    @Test
    @DisplayName("Update a customer")
    void getCustomerAndUpdate() {
        // Given
        Optional<Customer> customers = customerDao.get(customerId);

        // When
        Customer tmp = customers.get();
        tmp.setAge(99);
        customerDao.update(tmp);

        // Then
        Optional<Customer> tmp2 = customerDao.get(customerId);
        assertThat(tmp2.get().getAge(), is(99));
    }

    @Test
    void getAllCustomers() {
        // Given
        Customer customer = new Customer("Jochen", "Meyer", 99);
        customerDao.save(customer);

        // When
        List<Customer> customers = customerDao.getAll();

        // Then
        assertThat(customers.size(), is(2));
    }

    private static String createUUID() {
        return UUID.randomUUID().toString();
    }
}
